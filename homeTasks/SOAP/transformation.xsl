<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>

    <xsl:template match="/*">
        <csobApiResponse>
                <xsl:for-each select="clients/*">
                        <client>
                            <clientId> _ <xsl:value-of select="./id"/> / <xsl:value-of select="./name"/> _ </clientId>

                            <clientType>
                                <xsl:value-of select="./clientType"/>
                            </clientType>

                            <dateOfRegistration>
                                <xsl:value-of select="./dateOfRegistration"/>
                            </dateOfRegistration>

                            <contactData> / <xsl:value-of select="./contactData/userAccount"/> (<xsl:value-of select="./contactData/phone"/>) <xsl:value-of select="./contactData/email"/> / </contactData>

                        </client>
                </xsl:for-each>
        </csobApiResponse>
    </xsl:template>
</xsl:stylesheet>